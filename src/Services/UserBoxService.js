import {request} from "./UtilService";

//EndPints
const USER_DETAIL_SMART = '/userdetailSmart';
const USER_DETAIL = '/userdetail';


//APIs
export async function userDetailsAPI(USER_BASE_API, type) {

  if (type === 'SMART') {
    return await request({
      url: USER_BASE_API + USER_DETAIL_SMART,
      method: 'get',
    });
  } else if (type === 'user') {
    return await request({

      url: USER_BASE_API + USER_DETAIL,
      method: 'get',
    });
  }
}
