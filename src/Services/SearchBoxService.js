import {request} from './UtilService';
//EndPoints
const FETCHMODULESBYQUERYPARAM_API = '/fetchModulesByQueryParam';

//APIs
export async function makeAndHandleRequestOld(USER_BASE_API, query, page = 1) {

  const SEARCH_URI = USER_BASE_API + FETCHMODULESBYQUERYPARAM_API;

  return await request({
    url: `${SEARCH_URI}?query=${query}&page=${page}&perPage=50`,
    method: 'GET'
  });
}
