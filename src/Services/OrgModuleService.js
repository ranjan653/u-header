import {request} from './UtilService';


//EndPoints
const DASHBOARD_API = '/dashboard';

//APIs
export async function getOrgModulesAPI(USER_BASE_API) {

  return await request({

    url: USER_BASE_API + DASHBOARD_API,
    method: 'GET'
  });
}
