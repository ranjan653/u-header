import {request} from './UtilService';

//EndPoints
const GET_TIMEZONES_API = '/getTimezones';

//APIs
export async function getTimezonesAPI(USER_BASE_API) {

  return await request({
    url: USER_BASE_API + GET_TIMEZONES_API,
    method: 'GET'
  });
}
