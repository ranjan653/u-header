import {request} from './UtilService';

//Endpoints
export const WHITE_LABELING = '/whiteLabeling';

//APIs
export async function whiteLabelingAPI(USER_BASE_API) {

  return await request({

    url: USER_BASE_API + WHITE_LABELING,
    method: 'GET'
  });
}
