import Cookies from "universal-cookie";

const axios = require('axios');
export const request = (options) => {

  const config = {
    headers: {'Content-Type': 'application/json'},
    url: options['url'],
    method: options['method'],
    data: options['body']
  };

  const cookies = new Cookies();
  if (cookies.get('access_token', {httpOnly: false})) {

    config['headers']['Authorization'] = 'Bearer ' + cookies.get('access_token', {httpOnly: false});
  }
  return axios.request(config).then(
    response => {

      if (parseInt(response.status, 10) === 200) {

        return response.data;

      } else {
        return Promise.reject(response);
      }
    }
  );
};
