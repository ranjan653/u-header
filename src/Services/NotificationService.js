import {request} from "./UtilService";

//Endpoints
const NOTIFICATION_LIST_AFTER_LOGIN = '/showNotification';
const NOTIFICATION_BY_ID_AFTER_LOGIN = '/fetchNotificationDetailsById';
const NOTIFICATION_READ = '/saveNotificationAsRead';
const NOTIFICATION_LIST_AFTER_LOGIN_EVENT = '/showNotificationEvent';
const DOWNLOAD_NOTIFICATION_ATTACHMENT = '/downLoadReactAttachment';
//APIs
export async function notificationAfterLoginAPI(USER_BASE_API, count) {

  let query = '?count=' + count;
  return await request({

    url: USER_BASE_API + NOTIFICATION_LIST_AFTER_LOGIN + query,
    method: 'GET'
  });
}

export async function notificationAfterLoginEventAPI(USER_BASE_API, count) {

  let query = '?count=' + count;
  return await request({

    url: USER_BASE_API + NOTIFICATION_LIST_AFTER_LOGIN_EVENT + query,
    method: 'GET'
  });
}

export async function notificationByIdAfterLoginAPI(USER_BASE_API, id) {

  let query = '?notificationId=' + id;
  return await request({

    url: USER_BASE_API + NOTIFICATION_BY_ID_AFTER_LOGIN + query,
    method: 'GET'
  });
}

export async function notificationReadByIdAPI(USER_BASE_API, id) {

  let query = '?notificationId=' + id;
  return await request({

    url: USER_BASE_API + NOTIFICATION_READ + query,
    method: 'GET'
  });
}

export async function downloadNotificationAttachmentAPI(AUTH_BASE_API, id) {

  let query = '?notificationId=' + id;
  return await request({

    url: AUTH_BASE_API + DOWNLOAD_NOTIFICATION_ATTACHMENT + query,
    method: 'GET'
  });
}
