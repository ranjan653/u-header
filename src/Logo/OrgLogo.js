import React, {Fragment} from 'react';
import Loader from 'react-loaders'
import Avatar from "react-avatar";
import {userDetailsAPI} from '../Services/UserBoxService';

export default class OrgLogo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      active: this.props.active,
      user: {},
      horizontalLogo: false,
      loading: false
    };
  }

  getUserDetails = () => {
    this.setState({
      loading: true
    });
    userDetailsAPI(this.props['userApi'], 'user')
      .then(response => {
        this.setState({
          user: response.profile,
          horizontalLogo: response.horizontalLogo,
          loading: false
        })
      });
  };

  componentDidMount() {
    if (this.props['userApi'] && this.props['userApi'] !== '') {

      this.getUserDetails();
    } else {
      console.error("Please provide valid props in component: userApi");
    }
  }

  render() {

    return (

      <div>
        {this.state.loading ? <Loader innerClassName={"ml-4"} color="#ffffff" active type={"ball-clip-rotate"}/> :
          <span>
            {this.state.user.logo ?
              this.state.horizontalLogo ?
                <img src={this.state.user.logo}
                     alt="logo"
                     height="31px" width="31px"/>
                :
                <img
                  alt="logo"
                  src={this.state.user.logo}
                  height="31px" width="136px"/>
              :
              <span><Avatar style={{maxWidth: '136px', width: '136px'}} textSizeRatio={1.5}
                            name={this.state.user['client_name']} size={'43'}/></span>
            }
          </span>
        }
      </div>
    )
  }
}
