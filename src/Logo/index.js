import React from 'react';
import Avatar from 'react-avatar';
import {whiteLabelingAPI} from "../Services/LogoService.js";
import Loader from "react-loaders";

export default class Logo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      whiteLabeling: '',
      horizontalLogo: false,
      loading: false,
    }
  }

  loadWhiteLabeling = () => {

    this.setState({
      loading: true
    });
    whiteLabelingAPI(this.props['userApi'])
      .then(response => {
        this.setState({
          whiteLabeling: response['data']['rootWhiteLabeling'],
          horizontalLogo: response['data']['rootWhiteLabeling']['horizontalLogo'],
          loading: false
        });
      })
  };

  componentDidMount() {

    if (this.props['userApi'] && this.props['userApi'] !== '') {

      this.loadWhiteLabeling();
    } else {
      console.error("Please provide valid props in component: userApi");
    }
  }

  render() {

    let {whiteLabeling} = this.state;
    return (

      <span>
        <div className="ml-3">
          {this.state.loading ?
            <Loader active type={"ball-clip-rotate"}/>
            :
            <span>
          {whiteLabeling['logoUrl'] ?
            <span>
              {!this.state.horizontalLogo ?
                <a href={whiteLabeling['clickeableText']} target="_blank"><img
                  alt="logo"
                  src={whiteLabeling['logoUrl']} height="31px" width="31px"/></a>
                :
                <a href={whiteLabeling['clickeableText']} target="_blank"><img
                  alt="logo"
                  src={whiteLabeling['logoUrl']} height="31px" width="136px"/></a>
              }
             </span>
            :
            <span>
              <Avatar name={whiteLabeling['logoText']} size={'43'}/>
              </span>
          }
            </span>
          }
        </div>
      </span>
    );
  }
}
