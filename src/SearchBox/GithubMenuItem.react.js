import React from 'react';

const GithubMenuItem = ({user}) => (
    <div>

        <i className={user.icon} style={{
            height: '24px',
            marginRight: '10px',
            width: '24px',
        }}/>
        <a href={user.moduleUrl} >{user.login}</a>
    </div>
);


export default GithubMenuItem;
