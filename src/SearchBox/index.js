import React, {Fragment} from 'react';

import cx from 'classnames';
import {Typeahead, asyncContainer} from 'react-bootstrap-typeahead';
import GithubMenuItem from './GithubMenuItem.react';
import {makeAndHandleRequestOld} from '../Services/SearchBoxService';

const AsyncTypeahead = asyncContainer(Typeahead);


class SearchBox extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activeSearch: false,
      multiple: false,
      placeholder: "",
      selectedOption: null,
      value: '',
      options: [],
      suggestions: [],
      selections: [],
      isLoading: false,
      option: [],
      query: '',
    };
  }

  onChange = (event, {newValue}) => {

    this.setState({
      value: newValue
    });
  };

  handleModuleSearch = (selected) => {

    window.location.href = selected[0]['moduleUrl'];
  };

  state = {
    isLoading: false,
    options: [],
    query: '',
  };


  _cache = {};

  _handleInputChange = (query) => {
    this.setState({query});
  };

  _handlePagination = (e, shownResults) => {
    const {query} = this.state;
    const cachedQuery = this._cache[query];
    if (cachedQuery && cachedQuery.data &&
      cachedQuery.data.options && (
        cachedQuery.data.options.length > shownResults ||
        cachedQuery.data.options.length === cachedQuery.data.options.total_count)
    ) {
      return;
    }

    this.setState({isLoading: true});

    const page = cachedQuery.page + 1;
    //this.makeAndHandleRequest

    makeAndHandleRequestOld(this.props['userApi'], query, page)
      .then((resp) => {

        const options = cachedQuery.data.options.concat(resp.data.options);
        this._cache[query] = {...cachedQuery, options, page};
        this.setState({
          isLoading: false,
          options,
        });
      });
  };

  _handleSearch = (query) => {

    if (this._cache[query]) {

      this.setState({options: this._cache[query].data.options});
      return;
    }

    this.setState({isLoading: true});
    makeAndHandleRequestOld(this.props['userApi'], query)

      .then(({data}) => {
        const options = data.options;
        this._cache[query] = {data, page: 1};

        this.setState({
          isLoading: false,
          options
        });
      });

  };

  async componentDidMount() {

    if (this.props['userApi'] && this.props['userApi'] !== ''
      && this.props['authApi'] && this.props['authApi'] !== ''
      && this.props['authUrl'] && this.props['authUrl'] !== '') {

    } else {
      console.error("Please provide valid props in component: userApi, authApi, authUrl");
    }
  }

  handleSearchButtonOnClick = () => {
    if (this.state.activeSearch) {
      this.setState({activeSearch: !this.state.activeSearch, placeholder: ''});
      this.typeahead.getInstance().clear()
    } else {
      this.setState({activeSearch: !this.state.activeSearch, placeholder: 'Type to search'})
    }
  };

  handleCloseButtonOnClick = () => {
    this.setState({activeSearch: !this.state.activeSearch, placeholder: ''});
    this.typeahead.getInstance().clear()
  };

  render() {

    return (
      <Fragment>

        <div className={cx("search-wrapper", {
          'active': this.state.activeSearch
        })}>
          <div className="input-holder" style={{overflow: "visible"}}>
            <AsyncTypeahead
              id="seachBox2"
              {...this.state}
              filterBy={['name', 'description', 'moduleUrl']}
              labelKey={option => `${option.name}`}
              maxResults={50 - 1}
              minLength={2}
              onInputChange={this._handleInputChange}
              // onChange={(selected) => {
              //   this.handleModuleSearch(selected)
              // }}
              onPaginate={this._handlePagination}
              onSearch={this._handleSearch}
              className="custom_select_input"
              paginate
              placeholder={this.state.placeholder}
              renderMenuItemChildren={(option) => (
                <GithubMenuItem key={option.id} user={option} onClick={(option) => {
                  window.open(option['moduleUrl'])
                }}/>
              )}
              ref={(typeahead) => this.typeahead = typeahead}
              useCache={false}
            />
            <button onClick={this.handleSearchButtonOnClick}
                    className="search-icon"
                    style={{marginTop: "-45px", marginRight: "0px", width: "42px", height: "42px"}}><span/></button>
          </div>
          <button onClick={this.handleCloseButtonOnClick} className="close"/>
        </div>
      </Fragment>
    )
  }
}

export default SearchBox;
