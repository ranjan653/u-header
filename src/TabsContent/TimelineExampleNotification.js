import React, {Component, Fragment} from 'react';

import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {notificationAfterLoginAPI} from "../Services/NotificationService";

export default class ChatExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // modal: false,
      notificationList: [],
      header: '',
      title: '',
      time: '',
      logo: '',
      cat: '',
      description: '',
      attachment: '',
      notes: '',
      count: 0,
      counter: 1,
      flag: false,
      flag1: false,
      notificationId: '',
      callMethod: true,
    }
  }

  notificationAPI = (count) => {
    notificationAfterLoginAPI(this.props['userApi'], count)
      .then(response => {
        let oldList = this.state.notificationList;
        let newList = response.data['NotificationList'];
        if (newList.length < 10) {

          this.setState({

            callMethod: false,
          })
        }
        newList = oldList.concat(newList);
        this.setState({
          count: this.state.count + 10,
          notificationList: newList
        })

      })
  };

  showMorNotificationEvent = async () => {
    if (this.state.callMethod) {

      await this.notificationAPI(this.state.count + 10);
    }


  };

  componentDidMount() {

    notificationAfterLoginAPI(this.props['userApi'], this.state.count)
      .then(response => {

        this.props.changeNumberOfNotifications(response.data['unReadCountList'][0]['root_notification_count']);
        this.setState({
          notificationList: response.data['NotificationList'],
          flag: true,
        })

      });

  }

  render() {
    return (
      <Fragment>
        <div className="scroll-area-md" id="test">
          <PerfectScrollbar onYReachEnd={() => this.showMorNotificationEvent()}>
            <div className="p-3">
              <VerticalTimeline layout="1-column" className="vertical-without-time">
                {this.state.notificationList &&
                this.state.notificationList.map(items => {
                  return <div onClick={() => this.props.openModal(items)}>
                    <VerticalTimelineElement style={{cursor: 'pointer'}}
                                             className="vertical-timeline-item"
                                             icon={<i className="badge badge-dot badge-dot-xl badge-primary"> </i>}
                    >
                      <h4 className="timeline-title">{items.title}&nbsp;&nbsp;
                        {!(parseInt(items['read_status'], 10) === 1) ?
                          <div className="badge badge-dot badge-dot-sm badge-danger">n </div>
                          : null
                        }
                      </h4>
                      <p>
                        {items['category_logo'].split(',').map(i => {
                          return <i className={i}> </i>
                        })}{' '}
                        {items['created_date']}

                      </p>

                    </VerticalTimelineElement>
                  </div>
                })}

              </VerticalTimeline>
            </div>
          </PerfectScrollbar>
        </div>
      </Fragment>
    )
  }
}

